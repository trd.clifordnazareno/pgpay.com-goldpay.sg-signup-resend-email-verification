<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat:700' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<style>
		@import url(//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css);
		@import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
	</style>
	<link rel="stylesheet" href="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/default_thank_you.css">
	<script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/jquery-1.9.1.min.js"></script>
	<script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/html5shiv.js"></script>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
		</br>
		<div class="alert alert-success" id="showwarning">
    <strong>You</strong> Send Verification Successfully
  </div>

<div style="width: fit-content;margin: auto;padding: 10px;"><a href="index.php">Register</a>&nbsp&nbsp&nbsp&nbsp<a href="login.php">Login</a></div>

	<header class="site-header" id="header">
		<h1 class="site-header__title" data-lead-id="site-header-title">SORRY YOU ARE NOT VERIFIED USER!</h1>
	</header>

	<div class="main-content">
		<i class="fa fa-warning main-content__checkmark" id="checkmark"></i>
		<p class="main-content__body" data-lead-id="main-content-body">Please verify your account now by clicking VERIFY NOW.</p>
	</div>
	<div id="ver">
	<p>Didn't receive the email verification? </p>


	<button id="demo">Verify Now</button>
		</div>

	<footer class="site-footer" id="footer">
		<p class="site-footer__fineprint" id="fineprint">Copyright ©2022 | All Rights Reserved</p>
	</footer>
</body>
</html>


<script>


document.getElementById("showwarning").style.display = 'none';

$(document).ready(function(){
  $("#demo").click(function(){
    $.get("http://localhost/pay/resend.php?email=<?php echo $_GET['ver']; ?>", function(data, status){
		document.getElementById("showwarning").style.display = 'inline';
		document.getElementById("demo").style.display = 'none';
		document.getElementById("ver").style.display = 'none';
		setInterval(myTimer, 120000);
    });



	
  });
});

document.getElementById("demo").style.display = 'inline';
document.getElementById("ver").style.display = 'inline';



function myTimer() {
  const date = new Date();
  document.getElementById("demo").style.display = 'inline';
  document.getElementById("ver").style.display = 'inline';
  document.getElementById("showwarning").style.display = 'none';
}
</script>