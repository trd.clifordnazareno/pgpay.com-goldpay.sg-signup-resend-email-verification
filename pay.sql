-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2022 at 09:51 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pay`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `status` enum('1','2','3') NOT NULL DEFAULT '1',
  `vercode` varchar(255) DEFAULT NULL,
  `dtstamp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `status`, `vercode`, `dtstamp`) VALUES
(1, 'asdasdasd@aadadas.com', 'a', '3', NULL, NULL),
(2, 'zasdasd@dsddsads.com', 'sadasdasdasda', '1', NULL, NULL),
(3, 'John', 'Doe', '', NULL, NULL),
(4, 'Mary', 'Moe', '', NULL, NULL),
(5, 'Julie', 'Dooley', '', NULL, NULL),
(6, 'a', 'a', '1', NULL, '1665052972'),
(7, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665053003'),
(8, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665110728'),
(9, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665110930'),
(10, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665111882'),
(11, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665111935'),
(12, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665112092'),
(13, 'trd.clifordnazareno@gmail.com', 'a', '1', NULL, '1665112158'),
(14, 'trd.clifordnazareno@gmail.com', 'a', '1', 'effb5c38c78cf94b07d2ceb25b2a61ab', '1665112672'),
(15, 'trd.clifordnazareno@gmail.com', 'a', '1', 'f841b90eff7eea08d4ae93d1bce5818a', '1665112709'),
(16, 'trd.clifordnazareno@gmail.com', 'a', '2', 'd51d75f041ad7eb2da9e816d668df413', '1665112890'),
(17, 'dbzepisode01@gmail.com', 'a', '2', '787e1049aca0361f02bf561cf8d20cc8', '1665115394'),
(18, 'test000', 'a', '2', '9be217b6a2ab18d82128ffdd5be7cbce', '1665117268'),
(19, 'test000@gmails.com', 'a', '1', '09f7c60a92660119cbbbed41ec0dad81', '1665118468'),
(20, 'asdasdas', 'a', '1', 'e17e75dbf7f05d5c1b32b7b234fada98', '1665118936'),
(21, 'adsdasdasdas', 'a', '1', 'dd794e58a347dfd8b91e9924f2605ddd', '1665119480'),
(22, 'asdasdasdas', 'a', '2', '6eb6699be6e913e6ec88bc5100fb33b8', '1665119592'),
(23, 'adasdasdas', 'a', '1', 'c1e7bbf7e336c0c1a26ae9af9aef1cc9', '1665119683'),
(24, 'asdasdasd', 'a', '2', '4e95d06688c9eda13d8be0b6ba229ded', '1665119785'),
(25, 'asdasdadas@gmail.com', 'a', '2', '6249572b4799c5e1003e4705207b2790', '1665120671');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
